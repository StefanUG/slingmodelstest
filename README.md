Sling Models Test
========

This a content package project generated using the multimodule-content-package-archetype.

Noteworthy
----------

The noteworthy parts are:

* The HelloBean java class, which is a sling model
* The custom Injector that can inject the ToolbarConfig (which is only an interface)
* The (very simple) hello-component, which uses <sling:adaptTo /> to make an instance of the HelloBean in the JSP
  (NOTE: This requires an updated version of the Sling taglib, which the content package is embedding as well)

Dependencies
------------

It depends on the two packages from Adobe Consulting Services:

* [ACS Embedded Sling Models API](https://github.com/Adobe-Consulting-Services/com.adobe.acs.bundles.sling-models)
  Package, which embeds the two bundles: Sling Models Implementation and Sling Models API
* [ACS AEM Commons](https://github.com/Adobe-Consulting-Services/acs-aem-commons)
  Package, which comes with a number of commons functionality - including AEM relevant injectors that can inject Page, Design and other useful objects.
  [Read more here](http://adobe-consulting-services.github.io/acs-aem-commons/features/aem-sling-models-injectors.html)

Building
--------

This project uses Maven for building. Common commands:

From the root directory, run ``mvn -PautoInstallPackage clean install`` to build the bundle and content package and install to a CQ instance.

From the bundle directory, run ``mvn -PautoInstallBundle clean install`` to build *just* the bundle and install to a CQ instance.

Using with VLT
--------------

To use vlt with this project, first build and install the package to your local CQ instance as described above. Then cd to `content/src/main/content/jcr_root` and run

    vlt --credentials admin:admin checkout -f ../META-INF/vault/filter.xml --force http://localhost:4502/crx

Once the working copy is created, you can use the normal ``vlt up`` and ``vlt ci`` commands.

Specifying CRX Host/Port
------------------------

The CRX host and port can be specified on the command line with:
mvn -Dcrx.host=otherhost -Dcrx.port=5502 <goals>


License
-------

This software is licensed under the MIT License

Copyright (c) 2015 Stefan Grinsted

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.