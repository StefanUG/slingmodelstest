<%@page import="org.apache.sling.api.resource.Resource"%>
<%@page import="com.valtech.cq.slingmodelstest.HelloBean"%>
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling" %>
<%@include file="/apps/slingmodelstest/global.jsp" %><cq:defineObjects />
<sling:adaptTo adaptable="${resource}" adaptTo="com.valtech.cq.slingmodelstest.HelloBean" var="hello"/>
<h1>${ hello.pageTitle }</h1>
<p>Repo name is: ${ hello.repositoryName }</p>
<p>Hello is: ${ hello }</p>
<p>Greeting is: ${ hello.greeting }</p>
<p>Static Greeting is: ${ hello.staticGreeting }</p>
<p>Toolbar is: ${ hello.toolbar }</p>
<p>Toolbar title is: ${ hello.toolbar.title }</p>
<p>Toolbar subtitle is: ${ hello.toolbar.subtitle }</p>
