package com.valtech.cq.slingmodelstest;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.day.cq.wcm.api.Page;

@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.REQUIRED)
public class HelloBean {

	@Inject
	private HelloService service;
	
	@Inject
	private Page resourcePage;
	
	@Inject
	private String greeting;
	
	@Inject
	private ToolbarConfig toolbar;

	public String getRepositoryName() {
		return service.getRepositoryName();
	}
	
	public String getPageTitle() {
		return resourcePage.getTitle();
	}
	
	public String getGreeting() {
		return greeting;
	}
	
	public String getStaticGreeting() {
		return "Hello world!";
	}

	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}

	public ToolbarConfig getToolbar() {
		return toolbar;
	}

	public void setToolbar(ToolbarConfig toolbar) {
		this.toolbar = toolbar;
	}
	
}
