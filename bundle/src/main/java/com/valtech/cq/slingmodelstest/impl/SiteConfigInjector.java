package com.valtech.cq.slingmodelstest.impl;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.spi.DisposalCallbackRegistry;
import org.apache.sling.models.spi.Injector;

import com.valtech.cq.slingmodelstest.ToolbarConfig;


@Component
@Service
@Property(name = "service.ranking", intValue = 50)
public class SiteConfigInjector implements Injector {

	public String getName() {
		return "site-config";
	}

	public Object getValue(Object adaptable, String name, Type declaredType, AnnotatedElement element, DisposalCallbackRegistry callbackRegistry) {
		if (declaredType.equals(ToolbarConfig.class)) {
			if (adaptable instanceof Resource) {
				String pagePath = ((Resource) adaptable).getPath();
				pagePath = getAbsoluteParent(pagePath, 2);
				
				StringBuilder configPath = new StringBuilder(pagePath);
				configPath.append("/toolbar/jcr:content");
				
				Resource toolbar = ((Resource) adaptable).getResourceResolver().getResource(configPath.toString());
				return toolbar.adaptTo(ToolbarConfig.class);
			}
		}
		return null;
	}

	
	public static String getAbsoluteParent(String path, int level) {
		int idx = 0;
		int len = path.length();
		while ((level >= 0) && (idx < len)) {
			idx = path.indexOf(47, idx + 1);
			if (idx < 0) {
				idx = len;
			}
			--level;
		}
		return ((level >= 0) ? "" : path.substring(0, idx));
	}	
	
	
}
